var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var feedsRouter = require('./routes/feeds');
var app = express();
var newFeedRouter = require('./routes/newFeed');
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'client','build')));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/api/feeds', feedsRouter);
app.use('/api/newFeed', newFeedRouter);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
let bdURL = 'mongodb+srv://Natalia:1234@cluster0.ov9r8.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
mongoose.connect(bdURL, { useNewUrlParser: true, useUnifiedTopology: true }, function (err) {


  if (err) throw err;

  console.log('БД подключена');
});

module.exports = app;
