var express = require('express');
var router = express.Router();
const config = require('config');
const blackList = config.get('blacklist')
const FeedsModel = require('./../src/model/feedbacks');

router.post('/', function (req, res, next) {
  res.set('Content-Type', 'application/json');

  const text = req.body.feedback.toLocaleLowerCase()
  let badContent = false
  blackList.forEach((elem) => {
    if (text.indexOf(elem) >= 0) {
      badContent = true
      res.send('err');
    }
  })

  if (!badContent) {

    const document = {};

    document.author = req.body.author;
    document.feedback = req.body.feedback;
    document.topic_id = req.body.topic_id;
    document.date = new Date().toLocaleString();

    FeedsModel.create(document, function (err, document) {
      if (err || document == null) {
        res.send('403');
        res.end()
        return
      }
      res.send(document)
    });
  }
});

module.exports = router;