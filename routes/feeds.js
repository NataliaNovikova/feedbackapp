var express = require('express');
var router = express.Router();
const FeedsModel = require('./../src/model/feedbacks');

router.post(`/`, function (req, res, next) {
  
  FeedsModel.find({ topic_id: req.body.id }, function (err, doc) {

    if (err || doc == null) {
      res.status(403)
      res.end()
      return
    }
    res.send(doc)
  });
});

module.exports = router;