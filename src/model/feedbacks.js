var mongoose = require('mongoose');

var FeedsSchema = new mongoose.Schema({
  date: { type: String },
  author: { type: String },
  feedback: { type: String },
  topic_id: { type: Number }
});


const FeedsModel = mongoose.model("Feed", FeedsSchema);
module.exports = FeedsModel;
